#!/usr/bin/env python3
# from wand.image import Image
# from wand.display import display
import glob
import os

types = ('**/*.png', '**/*.jpg')
files = []
for t in types:
	files.extend(glob.glob(t, recursive=True))

for file in files:
	# with Image(filename=file) as img:
	# 	print(img.size)
	# 	if img.width > 2000:
	# 		ratio = img.width / img.height 
	# 		newheight = 2000 / ratio
	# 		with img.clone() as i:
	# 			i.resize(2000, int(newheight))
	# 			i.save(filename=file)
				# display(i)
	renamed = file.replace(' ', '-')
	print(file)
	if renamed is not file:
		os.rename(file, renamed)