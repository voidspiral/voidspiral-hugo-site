# Voidspiral Hugo Website

* Hugo
* Gitlab
* Custom Theme
* https://purecss.io/

# Page Structure

* `Body`
  * `Header`
    * `H1` Site Name
    * `P` Page Name
    * `Nav`
      * Pages
  * `Main`
    * `Article`
      * `Header` Heading
        * Heading
        * Hero Image
        * Tags
      * `Section` Content
        * P
        * Img
        * blockquote
      * `Footer` Footer
        * Author
        * Comments
        * Other Tagged Posts
    * `Section` List
      * `article` Featured Post
        * `h2` heading
        * 
      * `article` Listed Post
        * 
  * Aside
    * Featured
    * Nav
      * Tagged
      * Socials
  * Footer
    * Back to Top
    * Socials
    * Info
    * Copyright

# voidspiral site structure

* All Pages
  * Head
    * Metadata
  * Header
    * primary nav horizontal
  * Footer
    * Social Links
    * page lists
    * product lines
* Home Page
  * Featured Product
    * Title
    * Image
    * Blurb
    * Large
  * Product Line List
    * Title
    * Image
    * Blurb
* Product List
  * Product
    * All posts tagged with product
* Posts (all news)
  * Post
* Page List (hidden)
  * Pages
* Author List
  * Author
    * Image
    * Website
    * Bio
    * Socials
* Partials
  * Primary Nav
  * Footer Link List Block
  * Author
    * Icon
    * Name
  * Social Icon List
    * 

# Notes, Links, Articles

* 