---
title: "Firefighter Sharks in Space"
date: 2019-05-17T12:48:06-05:00
draft: false
tags: [game, free]
featured: false
featured_image: "/images/ffsis-cover.jpg"
authors:
 - Richard Kelly
artists:
  - Joe Bush
---

Whooooah, dude!

Space is the most dangerous place for fires, and only sharks like you have the skills, guts, and fearsome drive to fight them!

In this dice-lite RPG, you and your friends can

be sharks
patrol space
bite fires until they stop
save your little human buddies, regardless of how much they want to be saved
There are also mechanics revolving around creativity, problem-solving, and the timely use of shark facts, making this system runnable for gamers ages 5 and up.

## Assets

- Get Firefighter Sharks in Space
  - [DriveThruRPG: Digital](https://www.drivethrurpg.com/product/280263/Firefighter-Sharks-in-Space)