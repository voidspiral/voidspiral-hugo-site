---
title: "Spiral Game System"
date: 2022-03-07T07:48:09-05:00
draft: false
featured_image: "/images/sgs/cover.jpg"
featured: false
tags: [sgs, core, game]
logo: "/images/sgs/sgs-logo-darkbg@3x.png"
authors:
  - Joe Bush
artists:
  - Joe Bush
aliases:
  - /sgs/
---

Our new flagship game system, complete with a new website for building and running your games!

<!--more-->

## Overview

The Spiral Game System is a system for building games. You can consider it a game engine, much like the D20 system.

We are using the SGS engine to build several new games, including Antum and The Wytchheart Machine.

SGS games can be run using our [online game app](https://sgs-web.herokuapp.com/), or on old-school paper-and-pencil.

## Resources

- [Online Game App](https://sgs-web.herokuapp.com/)
  - design, build, and run your SGS games online!
- [System Description](https://voidspiral.gitlab.io/sgs-apps/docs/)
  - read the rules and the system
- [Youtube Tutorial Playlist](https://www.youtube.com/playlist?list=PLcf3les-baVGAY9-fu3_Y-rY46_csiOAy)

## Key Features

- **flexibility**
  - You can build pretty much anything you can imagine, as long as you keep it within the bounds of the game you're playing
  - no more wrestling with character vs rules
  - Want to make an orcish wizard? Go right ahead!
- **balance**
  - Characters, monsters, items, vehicles, and powers all have *descriptive* values, not *prescriptive* values.
  - That means you can always figure out exactly how much something is worth and how strong the characters and monsters are.
- **efficiency**
  - SGS itself has a small number of unique mechanics but ensures that those mechanics are flexible enough to be used in a wide variety of situations.
  - instead of hundreds of specific rules you'll have to look up or memorize, each item, power, and monster is self-contained, so you have everything you need to run the critter in the stat block.


## How It Works

In broad strokes, SGS games work similarly to most other RPGs. There are characters, monsters, items, and occasionally vehicles. These are all considered "entities" and all entities have the same set of possible stats and their values are calculated the same way.

Entities have the following categories of stats:

- **Vitals:** Things that define the entity, like the name, description, Aspects, limitations, and XP value.
- **Primary Stats:** These are usually reserved for characters and monsters. They influence the entity's skill values, and thus how good they are at broad categories of activity.
- **Tactical Stats:** These define how far a thing can move or reach, or how well armored or warded it is. Tactical stats are more often found on weapons, armor, and powers.
- **Skills:** These are the basic tools a character uses to influence the game world and interact with others. Skills allow you to attack, evade, cast spells, solve mysteries, create things, or take advantage of situations. Skills generally get a bonus from an associated Primary Stat.
- **Thresholds:** These are a character's defenses. These are numbers that have to met or exceeded on an attack roll in order to deal damage to the entity. Each threshold is associated with a pool and an amount of damage to that pool.
- **Pools:** are the resources a character uses in conflicts, like health and mana. Some pools, like mana, are spent to perform spells or powers. Other pools, like health, are reduced by incoming damage. Pools usually determine how long a character can keep fighting.
- **Equipment:** is the set of other entities that a character has access to. Because SGS is flexible, equipment can even contain other creatures or assistants, such as familiars, pets, mounts, vehicles, or trusted lieutenants.

The core mechanic of an SGS game depends on that game's rules, but generally you'll be rolling a die, adding a Skill value, and the value of a Primary stat associated with that skill. That number is then compared against a target number or a ladder to determine success.

If you're making an attack roll, you'll usually add the die of the weapon you're using directly to the attack roll. The attack roll is then compared to the target's Thresholds, and if they're met, you deal damage to that target. The number of boxes of damage you deal is equal to the number of thresholds you beat. If you've taken damage boxes, you take a penalty on your rolls equal to the number of boxes.

In addition to the normal skill-based resolution mechanic, SGS also features a system of aspects, flaws, and tokens similar to the Fate system.

![sgs logo](/images/sgs/sgs-logo-darkbg@3x.png)