---
title: "The Lost Soul Blues"
date: 2019-06-14T12:48:06-05:00
draft: false
featured: false
tags: [game, free]
featured_image: "/images/tlsb-cover.jpg"
logo: "/images/o2e-logo.png"
authors:
  - Joe Bush
artists:
  - Joe Bush
---

The Lost Soul Blues is a short, compact game about lost souls trying to earn their freedom from Hell. Don't let the 3 pages fool you, there's rules for the players, rules for the GM, and even mods and rules for advancement.

The Lost Soul Blues is in Beta, and we need your help to refine it. Join the discussion on reddit.com/r/voidspiral and tell us what you think!

## Assets
- [DriveThruRPG](https://www.drivethrurpg.com/product/280088/The-Lost-Soul-Blues)
- [Direct Download](/downloads/other/The_Lost_Soul_Blues.pdf)