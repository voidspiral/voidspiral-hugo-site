---
title: "Wasure­mono­gatari"
date: 2014-08-07T12:48:09-05:00
draft: false
featured: false
tags: [wa, game, out-of-print]
featured_image: "/images/wa/wa-cover.jpg"
logo: "/images/o2e-logo.png"
oop: true
authors:
  - Joe Bush
artists:
  - Joe Bush
---

An exotic, collaborative, open-source, and action-oriented game designed from the ground up for anime-style storytelling and combat.

![A white shiro castle](/images/wa/env_wide-angle.jpg)

Wa is built around the finest, most tried-and-true, sword-tested tropes from anime and manga. In Wa, you'll see beast people, katana-wielding samurai, ki-powered ninja, spiritually enlightened shugenja, ancient mountain hermits, and powerful blood ties that go back for millennia. Combat is explosive, quick, and merciless. Your heroes will master powers of your choosing, and wield them against a vast array of foes, from possessed paper lanterns to powerful samurai generals to conniving corrupt kami.

![A clash between katana and tetsubo](/images/wa/actions.png)

Wa thrives on intense, rapid combat, active, emotional characters, and deep, meaningful storylines. Wa is eschews sword-and-sorcery and dungeon crawling in favor of twisted tales of revenge, love, betrayal, and martial prowess.

Wa's game system is unique. It walks the line between effect-based and outcome-based systems, providing a general framework for how attacks and defenses should work, while allowing you to describe the action yourself.

The main mechanic in Wa is an opposed Skill Roll that the player can get bonuses on for interesting role-playing and well described combat techniques. This means that while there is a random element to combat, players who really need to succeed on a roll can tilt the odds in their favor by making the game interesting for everyone else!

![should I choose magic or poison and guile?](/images/wa/decisions.png)

Creative role-playing and clever maneuvers are rewarded in Wa. Coming up with a way to outsmart your opponent nets you bonuses to your rolls, providing a quick, sturdy mechanic that reinforces good roleplaying. It wouldn't feel like an anime if people weren't kicking boulders at each other and cutting down castles.

Many americanized versions of ninja and samurai are depicted as being historical or pseudo-historical. This is similar to having a roleplaying game about knights with no dragons, trolls, mages, or elves. Wa is not pseudo-historical. It's far more super-heroic, with powers, abilities, and fell magics vastly superior to mortal abilities. Don't argue about whether your samurai could take a Hopelite. If you're going to argue power, match yourself against Shishio or Sasuke or Gennosuke. We're aiming for a game that would make Nobuhiro Watsuki, Hideaki Anno, and Fumikane Shimada proud.

![One samurai rises, another falls](/images/wa/villains.jpg)

## Assets

- Buy Wasuremonogatari
  - [DriveThruRPG](https://www.drivethrurpg.com/product/133678/Wasuremonogatari-the-Anime--Manga-RPG)
- Sheets
  - [Character Sheet](/downloads/wa/Wa_Character_Sheet.pdf)
  - [Campaign Sheet](/downloads/wa/Wa_Character_Sheet.pdf)