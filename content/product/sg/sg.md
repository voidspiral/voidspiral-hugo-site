---
title: "Summoner's Grimoire"
date: 2014-12-03T12:48:09-05:00
draft: false
featured_image: "/images/sg/sg-cover.jpg"
featured: false
tags: [sg, core, game]
logo: "/images/o2e-logo.png"
authors:
  - Joe Bush
artists:
  - Joe Bush
---

Summoner's Grimoire is the best kept secret of the occult world. It was mentioned in the Necronomicon, the Book of Enoch, and the Dead Sea Scrolls (all of whom gave it five stars). HP Lovecraft allegedly played Summoner's Grimoire with his 2-6 closest tentac-err... friends.

<!--more-->

![summoner's grimoire box](/images/summoners-grimoire-box.jpg)

## BASIC MODE

In the Basic Mode, players roll dice to maneuver two Avatars across The Cosmos (actually, a game board) with the intent to occupy the opposite Temple. It’s a lot more complicated than it sounds, because other players can Banish your pieces to Purgatory by landing on them. This mechanic is simple and fast to play, but requires a curiously high amount of strategy. Basic Mode games are pretty quick, and a player can sit down and play in a couple of minutes.

![The map of the cosmos](/images/map-outline-01.png)

## ADVANCED MODE

The Advanced Mode adds some extra mechanics and strategic details. While movement of Avatars is decreased to 1 Domain per Avatar per turn, there are now new Patron and Incantation cards that give you special powers. These powers consume Manna, awarded at the beginning of each turn. Each Patron also has an Attack and Defense, which are used to determine the outcome of combat between Avatars on the board.

![shyri rahul, the world eater](/images/54f67d5aa84ec28cf7f0a92ac2b99d5c_original.webp)

## THE HOLY WAR

Players still play against each other, but the goal is not to reach the Temple on the opposite side of the game board, the goal is now to either collect 20 Manna, or to be the last Avatar standing. You lose the game if you run out of Manna, or if both of your Avatars are Banished. Yes, that’s right, Avatars do not return from Purgatory in this game mode.

Players do not gain Manna at the beginning of the turn. Instead, they must defeat Nephilim on the board. Each monster is represented by a Manna token stack on each of the Domains. Players gain Manna depending on the Plane the Nephilim is on: Subtle Plane (the outermost): 1 Manna, Mortal Plane (the middle) 2 Manna, and the Supreme Plane (the innermost circle) 3 Manna. There are fewer of the more valuable Nephilim, so players must carefully balance speed versus safety, because the Nephilim fight back. Each has 1 Attack and Defense Die per point of Manna, making the Supreme Nephilim pretty dangerous. But even more valuable are the other Player’s Avatars, each worth 5 Manna. Combat works somewhat differently, with the possibility of nothing happening, or both attack and defense succeeding, resulting in gained Manna and Banishment both.

![summoner's grimoire cards and game components](/images/summoners-grimiore-cards.jpg)

## RAISING R’LYEH

In Raising R’lyeh, players take turns placing colored Nephilim tokens around their Temples. The objective is to defeat and collect one of each color and reach the Dead Dreaming Gate, but be careful, because you're playing a weak, squishy human instead of one of the servants of the gods. Every loss means you have to give up a sacrifice. Capture them all before anyone else!

![](/images/864ee70063227626edb7f5f513b6e3c6_original.webp)

## APOCALYPSE

Players start with 2 Avatars. They can summon 2 monsters per turn, into Domains occupied by monsters (hence stacking) or into any domain adjacent to a monster or Avatar. You can make 2 moves per turn. Monster stacks (armies) can move as one, or you can un-stack a number of them (i.e. move 3 monsters off a stack of 5).

When monsters fight, they reduce each other’s tokens. Monsters can kill Avatars, but Avatars can reduce monster stacks by their roll (so a roll of 4 removes 4 monsters from the stack). Avatar vs Avatar combat continues as normal.

The goal is to take the Dreaming Dead Gate, (guarded by 12 generic monsters represented by 2d6) and/or destroy all your opponents.

## Assets

- Buy Summoner's Grimoire
  - [TheGameCrafter](https://www.thegamecrafter.com/games/sg:-transcendent-editon)
- Addons
  - [Ars Regulis Card Pack](https://www.thegamecrafter.com/games/sg:-ars-regulis-card-pack)
  - [Dreamlands Board](https://www.thegamecrafter.com/games/sg:-dreamlands-board)
