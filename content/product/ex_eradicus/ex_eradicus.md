---
title: "Ex Eradicus"
date: 2022-12-21T8:48:06-05:00
draft: false
featured: true
tags: [eradicus, core, game, featured, sgs]
featured_image: "/images/eradicus/alpha-cover.jpg"
logo: "/images/eradicus/ex-eradicus-logo.png"
hidehero: false
authors:
  - Joe Bush
  - Richard Kelly
  - Casey Cotter
  - Twila Cotter
  - Chris Xavier
artists:
  - Joe Bush
  - Kateryna Hrystyshyna
  - Francesco Orru
aliases:
  - '/eradicus/'
  - '/eradicus'
  - '/product/eradicus'
  - 'ee'
  - '/ee'
---

> The worlds are dead. We stand on their ashes, gray clay underfoot, gray expanse of stone above. We are those left behind by the cataclysm, the debris of annihilation. Dust.
>
> <!--more-->
> 
> Some say that the clay and ash are aggregate souls, each fleck a lifetime or more. Now, eons after the cataclysm, the survivors have remade the land, shaping the clay and building pale reflections of their once-great universes. There is space aplenty. There is much time. The first to arrive have had spans innumerable to fashion new realms from the wasteland. Now they reach ever higher, building towers and cities to rival the dead heavens. As these realms are forged, new alliances and enemies are made.
> 
> Some fell through the cracks. Lost in the maelstrom, we passed into the abyss. If only one survived the death of each world, then that leaves an untold number, some fraction of infinity, roaming the wastes, searching for meaning. Or simply revenge.
> 
> Now many ally themselves together in bands, armies, even nations. Warlords rise and fall, heroes are forged from the fires of war and chaos. Loyalty is traded for power. Weapons are forged from iron sifted from rivers of tar. Spells are drawn forth from pools of chaotic energy. Artifices the like of which no mortal world would have ever permitted are fabricated in the halls of the ancient ones, and granted to their highest lieutenants. Creatures are shaped from the clay and sent into battle against them.
> 
> These are our times. Such is our world. This is Eradicus.

<!-- <a href="https://www.kickstarter.com/projects/josephleebush/ex-eradicus-into-the-gray-wastes?ref=a9t1lw" style="color: #05ce78; font-size: 2em; font-weight: bold; text-align: center;">CHECK OUT OUR KICKSTARTER NOW!</a> -->

[![Ex Eradicus Logo](/images/eradicus/ex-eradicus-logo.png)](https://www.kickstarter.com/projects/josephleebush/ex-eradicus-into-the-gray-wastes?ref=a9t1lw)

Ex Eradicus is a miniatures-agnostic coop skirmish game. Walking the line between wargame and RPG, Ex Eradicus allows you to bring whatever kind of hero you want to the table and provides a huge world for you to battle in alongside your allies.

If you've ever wanted to use a super-cool miniature, but had no games that allow it, Ex Eradicus is for you. Make use of your most outlandish creations, whether they're from MyMiniFactory, Thingiverse, HeroForge, Reaper Minis, Games Workshop, or anywhere else!

## Features

- Coop gameplay against complex AI opponents
- Skirmish-level battles
- Simple but powerful combat system
- Uses 1-3 sets of polyhedral dice
- Miniatures agnostic
- Completely customizable models, units, and powers
- Dozens of battle objectives
- Dozens of battle complications
- A wide array of enemy AI archetypes
- Detailed but fast rules for terrain, obstacles, and movement
- Vast and expanding lore

## We Need Your Support!

Help us make Ex Eradicus as awesome as possible! Support the development on [Patreon](https://www.patreon.com/voidspiral)!

**Patreon Posts about Ex Eradicus**

- [Design your Own Rules with Ex Eradicus on the Web App](https://www.patreon.com/posts/86395031)
- [Promo Video](https://www.patreon.com/posts/ex-eradicus-1-86095144)
- [About the Game](https://www.patreon.com/voidspiral/posts?filters%5Btag%5D=Eradicus)

## Assets

- Game Rules
  - <a href="https://www.drivethrurpg.com/en/product/428563/ex-eradicus-beta" target="_blank">Ex Eradicus Core Rules (PDF and Print on Demand)</a>
- Gameplay Cards
  - <a href="/downloads/ee/Eradicus print and play Duplex.pdf" target="_blank" download>Print & Play Gameplay Cards (duplex)</a>
  - <a href="/downloads/ee/Eradicus print and play front only.pdf" target="_blank" download>Print & Play Gameplay Cards (single-sided)</a>
  - Print on Demand Gameplay Cards: <a href="https://www.thegamecrafter.com/games/ex-eradicus-gameplay-cards" target="_blank">TheGameCrafter</a>
- Sheets
  - <a href="/downloads/ee/Eradicus Cheat Sheet.pdf" target="_blank" download>Gameplay Cheat Sheet</a>
  - <a href="/downloads/ee/Eradicus mini character sheets.pdf" target="_blank" download>Mini Character Sheets</a>
  - <a href="/downloads/ee/Eradicus Model Builder Sheet.pdf" target="_blank" download>Model Build Sheet</a>

## Around the Webs
- [Eradicus Discord Channel](https://discord.gg/hjWy66Ms58)
- [Voidspiral](https://voidspiral.com/eradicus)
- [ExEradicus.com](http://exeradicus.com)
- [Eradicus.net](http://eradicus.net)
- [Eradic.us](http://eradic.us)
- [Discord](https://discord.gg/hjWy66Ms58)