---
title: "Brevity"
date: 2014-08-06T12:48:09-05:00
draft: false
featured: false
tags: [brevity, game, out-of-print]
featured_image: "/images/brevity/brevity-cover.jpg"
oop: true
authors:
  - Joe Bush
artists:
  - Joe Bush
---

Brevity is a simple universal role-playing game system. It features one simple resolution mechanic and a unique combination-based set of statistics that make it easy to create any kind of character you want.

Brevity is designed to be a pinch hitter, a system you can use for any setting or idea you come up with. It's designed to be easy to set up, quick to learn, and fast to play, staying out of the way of your storytelling.

Brevity's strength lies in its combination-based character stats. Select a Source of power, a Aptitude, and a Skill, and roll your dice. You choose how you peform your actions, from mental-based magic attacks to devious subterfuge-oriented shooting.

Try Brevity today, and get on with playing.

## Assets

- Get Brevity
  - [DriveThruRPG: Digital](https://www.drivethrurpg.com/product/133607/Brevity--simple-universal-roleplaying)