---
title: "Oubliette Version 1"
date: 2008-09-10T12:48:09-05:00
draft: false
featured: false
tags: [oubliette, game, out-of-print]
featured_image: "/images/ov1/ov1-cover.png"
oop: true
authors:
  - Joe Bush
artists:
  - Joe Bush
---

Oubliette is a dramatic storytelling game that takes place in a unique, dangerous, mysterious, and chaotic world.

In the world of Oubliette, players take on the roles of inhabitants of Castle Oubliette, a gargantuan, sprawling, ramshackle metropolis composed of the forgotten remnants of the medieval world. All things that were forgotten end up here, from people to magics, artifacts, and even entire cultures.

Enter the world of Oubliette. What will you become?

## Assets

- Out of Print
  - [DriveThruRPG page](https://www.drivethrurpg.com/product/108985/Oubliette-Version-1)
- [Oubliette Second Edition](/product/o2e/o2e)