---
title: "D&D 5E Conditon Rings"
date: 2019-06-23T12:48:06-05:00
draft: false
tags: [dnd5e, tool]
featured: false
featured_image: "/images/condition-rings-cover.jpg"
artists:
  - Joe Bush
---

60 ring-shaped condition tokens for D&D 5e
60 ring-shaped condition tokens for D&D 5e, including several extras for spells and non-condition effects like Rage, Marked, and Hexed. Need more of a specific token? Just add a second sheet! Need even more than that? Check out our cardstock tokens so you can really grab a ton!

These chipboard tokens are nice and weighty and easy to manipulate on the table. They drop nicely onto miniatures and don't slide around as much as the cardstock ones. We recommend a clear multi-compartment storage container for keeping them.

## Included Tokens

- Bane: 1
- Bless: 1
- Blinded: 2
- Bloodied: 3
- Charmed: 2
- Deafened: 2
- Dodging: 3
- Exhaustion: 3
- Frightened: 3
- Grappled: 4
- Hexed: 1
- Hidden: 4
- Incapacitated: 3
- Invisible: 3
- Marked: 1
- On Fire: 3
- Paralyzed: 2
- Petrified: 2
- Poisoned: 3
- Prone: 3
- Raging: 1
- Restrained: 2
- Stunned: 2
- Surprised: 3
- Transformed: 1
- Unconscious: 2

## Assets

- TheGameCrafter
  - [Chipboard Condition Rings](https://www.thegamecrafter.com/games/d-d-5e-condition-rings-chipboard-)
  - [Cardstock Condition Rings](https://www.thegamecrafter.com/games/d-d-5e-condition-rings-cardstock-)

<sub>Note: These tokens are printed double sided, but drift may cut off the words on the back side. The front side should be legible regardless.</sub>
