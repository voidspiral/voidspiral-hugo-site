---
title: "The Dawnline: Feral States"
date: 2019-03-19T12:48:52-05:00
draft: false
tags: [tdl, expansion]
featured: false
featured_image: "/images/tdl/tdlfs-cover.jpg"
logo: "/images/tdl-logo.png"
hidehero: false
authors:
  - Richard Kelly
artists:
  - Joe Bush
  - Miguel Santos
  - Enmanuel Abarca
---

There are as many monsters in the world as there are dunes in the twilight.

Some are simple to deal with. They wear human faces and inhabit human bodies, and the only thing monstrous about them is that they hunger.

They can be bargained with, or else driven off with blade and fire.

Others are stranger.

Predatory cities, statues that masquerade as men, sapient clots of cacti, and husks of living daylight haunt the wastes.

Sometimes they can be killed.

Sometimes they can be avoided.

And if a village is lucky, sometimes they can even be forgotten.

---

Included in this Dawnline supplement is an expanded list of creatures, cataloging roughly forty more of the threats found on Janus.

There are also five Scenarios complete with mini-games and bonus content, two new short stories, an in-depth guide to designing your own monsters, as well as more artwork by the excellent Joe Bush and Miguel Santos.

So load your sidearm, summon your bloodscythe, and get ready for another foray into the world where daylight is a place that hunts you.


## Assets

- Buy The Dawnline: Feral States
  - [DriveThruRPG: Print & PDF](https://www.drivethrurpg.com/product/279583/The-Dawnline-Feral-States)
