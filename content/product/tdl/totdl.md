---
title: "Tales of The Dawnline"
date: 2019-03-19T12:48:52-05:00
draft: false
tags: [tdl, fiction]
featured: false
featured_image: "/images/tdl/totdl-cover.jpg"
logo: "/images/hfa-logo.png"
hidehero: false
authors:
  - Richard Kelly
  - Joshua Berkowitz-Geller
  - Pete Carrier
  - Julia Henken
  - Devon Oratz
artists:
  - Joe Bush
---

A mortal masters his fear.

A pair of vampires debates their village’s attachment to a mysterious creature.

A raider has a change of heart.

An odd child becomes a vital part of a community.

---

These are the Tales of the Dawnline.

A collection of ten short stories set in the desolate twilight universe of The Dawnline, Tales Of features fiction by Dawnline creator Richard Kelly, as well as pieces by Joshua Berkowitz-Geller, Pete Carrier, Julia Henken, and Devon Oratz.

Reading [The Dawnline](/product/tdl/tdl) is recommended, as some of these stories may not function without context, but readers who especially like post-apocalyptic gothic science-fantasy may be able to go in blind.