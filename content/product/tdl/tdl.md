---
title: "The Dawnline"
date: 2019-03-19T12:48:52-05:00
draft: false
tags: [tdl, core, game]
featured: false
featured_image: "/images/tdl/tdl-cover.png"
logo: "/images/tdl-logo.png"
hidehero: false
authors:
  - Richard Kelly
artists:
  - Joe Bush
  - Miguel Santos
  - Enmanuel Abarca
---

> To the West is the Dark, swarming with Sirens and their kind.
> 
> To the East is the Light, armageddon to all that it touches.
> 
> Between them is civilization, scattered and hungry.

The Dawnline is a tabletop roleplaying game about a group of vampires protecting a nomadic village on a world where day and night are places, and where all of society lives in the twilight in between.

<!--more-->

![Davio the vampire and a lurk facing off](/images/tdl/C7bbDXs.jpg)

The vampires depend on their village for blood and companionship. Without the humans, they would starve in the wasteland or forget themselves as they regress into feral states.

At the same time, the village depends on the vampires. The twilight they travel through is not safe. It is filled with strange ruins, desperate raiders, feral vampires, other villages, and creatures from the Dark ahead.

Only the vampires can protect the village, and only the village can shelter the vampires.

If their alliance comes apart, there will be nothing left of either of them but ash on the Dawnline.

![Davio the vampire overseeing his village](/images/tdl/XnZt8Eo.jpg)

Inspired by games like The Banner Saga, Darkest Dungeon, Ryuutama, Fate of the Norns, and novels like Vampire Hunter D, The Dawnline is a gothic desertpunk Oregon Trail where the players take the roles of immortal guardians defending a village as it treks endlessly through the twilight.

Play in The Dawnline is split into two phases, with the PCs going out to explore an interesting site or deal with an imminent threat, and then returning home to address problems in the village. In addition to creating their PCs, The Dawnline also has players create this village, and supporting and upgrading it is as much a part of gameplay as diving into ruins and fighting monsters. Both the vampires and the village can die, but no death ever ends the game. An unprotected village finds new guardians, and village-less vampires become the protectors of a new community.

The Dawnline uses a 3d6, roll-over dice system for quick conflict resolution, but the game mechanics prioritize decision-making over luck. Every PC has a moderately-sized, easily refillable pool of Willpower that they can spend to tilt the odds on any given roll, and the same pool is used to fuel special powers and abilities, to block attacks, to hit harder, to act faster, and to bend the odds in the PCs' favors. Of course, enemies have similar pools, and a PC that spends big on a single action is left with less in their pool to defend against an enemy's follow-up.

The Dawnline is suited to both spur-of-the-moment one-shots and long campaigns, and it has an ideal group size of 2 - 5 players.

For more Dawnline material, see the upcoming creatures and scenarios supplement [Feral States](/product/tdl/tdlfs), and the upcoming short story collection [Tales Of The Dawnline](/product/tdl/totdl).

<div class="threeup whiteback">

![an alqimic](/images/tdl/iGEr3Ql.png)
![a nightmother siren](/images/tdl/90FcNfi.png)
![a gemini](/images/tdl/Y5iqjSk.png)

</div>

## Kickstarter Video

<div class="iframe">
	<iframe src="https://www.youtube.com/embed/V7YquDv65bY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

## Assets

- Buy The Dawnline
	- [DriveThruRPG: Print & PDF](https://www.drivethrurpg.com/product/264784/The-Dawnline)
- Sheets
	- [Character Sheet](/downloads/tdl/The_Dawnline_character_sheet.pdf)
	- [Condition Cards](/downloads/tdl/The_Dawnline_condition_cards.pdf)
- Expansions
	- [Feral States](/product/tdl/tdlfs)
	- [Tales Of The Dawnline](/product/tdl/totdl)