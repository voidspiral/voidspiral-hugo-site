---
title: "Arcandio's Virtual Dice Roller"
date: 2025-02-10T10:48:09-05:00
draft: false
featured: true
tags: [avdr, app, featured]
featured_image: "/images/avdr/titlecard.png"
aliases: ["/avdr", "/avdr.html", "avdr"]
oop: true
authors:
  - Joe Bush
artists:
  - Joe Bush
---

AVDR is a sick new dice rolling app for android!

<!-- more -->

## Features
- Select from numerous dice sets, dice trays, lighting schemes, and special effects.
- Organize preset rolls into specific characters that you can easily switch between.
- Shake-to-toll, or tap the roll button to roll the selected dice.
- Physics-based rolling with randomized starting conditions.

<div style=>

![Screenshot](/images/avdr/screencap1.png)
![Screenshot](/images/avdr/screencap2.png)
![Screenshot](/images/avdr/screencap3.png)


## Roadmap
We have a ton of features planned, most of which are covered by the upcoming [Kickstarter Campaign](https://www.kickstarter.com/projects/josephleebush/239176346?ref=eb8obl&token=a029e308). You can get an idea of what we're planning on our [Github Page](https://github.com/arcandio/avdr/milestones?direction=asc&sort=title&state=open).

## Kickstarter
We'll be running a [Kickstarter Campaign](https://www.kickstarter.com/projects/josephleebush/239176346?ref=eb8obl&token=a029e308) soon, so make sure you bookmark it so you get notified as soon as it goes live. A lot of the rewards are limited in quantity!

Check out the [Kickstarter Campaign](https://www.kickstarter.com/projects/josephleebush/239176346?ref=eb8obl&token=a029e308)!

## Get The App
We're currently looking for testers. If you want to help test the app, shoot a message to dev at voidspiral dot com from the email account you use on Google Play. We'll send you a reply with a join link!