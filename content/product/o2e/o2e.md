---
title: "Oubliette Second Edition"
date: 2017-06-19T12:48:06-05:00
draft: false
featured: true
tags: [oubliette, core, game, featured]
featured_image: "/images/o2e/o2e-cover.png"
logo: "/images/o2e-logo.png"
aliases: ["/oubliette", "/oubliette.html", "o2e", "/product/o2e", "/o2e"]
authors:
  - Joe Bush
  - Richard Kelly
artists:
  - Joe Bush
---

> Your immortality is not in question. But what will you do with your new power now that you're trapped in the World of the Forsaken?

Oubliette. A dungeon with no way out. A place to put things to forget about them. A grand castle filled with destitute immortals. A world that is heaven, hell, or something in between.

<!--more-->

![various oubliette characters](/images/o2e/patreon-bg.png)

Throughout the ages, the power of belief has waned for certain subjects. Forgotten, they disappear from our world. But where do they go? What do they do when they get there? Oubliette is the story of these forgotten beings and fell powers.

- Uncover a twisted world where all myths are true
- Come to grips with immortality and eternity
- Find something to fight for
- Explore predatory libraries, man-eating forests, lakes of mercury, black-magic black markets, and eons of forgotten labyrinths
- Transform from a serf to a dragon

![a divine-caste shadow monster devouring a building](/images/o2e/divine.jpg)

## The Book

- Complete Fate Core rules for Oubliette
- Powered by Fate Core
- Massive index with more than 1300 pre-generated & topical Aspects
- 17 detailed racial groups
- More than 160 unique and specific story hooks
- 22 factions deeply integrated into the history of the Castle
- 14 entire districts of unusual and fantastical lands
- More than 430 pre-generated Stunts
- Bestiary with more than 250 denizens of Oubliette
- Includes a huge anthology of short stories set in Castle Oubliette
- Narrative-driven gameplay
- Story-oriented NPC/Creature creation rules
- Includes a Legendarium, Chronicle, & Glossary
- Creature lists organized by race, faction, caste, and skill focus
- Exactly one reference to Tina Fey's hit film, Preen Girls.
- Lots of words
- Several Numbers

![Doomsday, an Army Hunter, serves the Veiled King by destroing the enemies of Castle Oubliette](/images/o2e/doomsday-wide.png)

## The Unbroken

The majority of the denizens of Oubliette have been ground down by endless hardships and emotional trauma. But there are a rare few that refuse to give up, struggling ever onward, scratching their way up from the dregs to rise to the highest echelons of Oubliette society. Heroes some, villains many, the Unbroken are the movers and shakers of the world of Oubliette. These are the characters the players take on, the toughest, most determined people in the cosmos: those that would stand against eternity itself.

![A pair of oubliette denizens](/images/o2e/e139759e3d3350ae76fcf6bfcde1a203_original.webp)

The Unbroken come from many walks of life. Some were warriors in life, other arcanists or clerics, but all were skilled. They share only two common traits:

**ALL ARE VALUABLE.** There is something about every single one of the millions of people in Oubliette that makes them special, useful, powerful, or unique. Many brought with them magic and other fell skills that no longer exist in the World of Life.

**ALL ARE LOST.** No trace of any of these inhabitants can be found in the World of Life. Many died in hopeless situations and were never found, or passed into anonymity after their deaths. Others were forsaken by all that knew them, leaving no record behind. And a very small few sought out Oubliette, closing themselves off and removing themselves from the world intentionally, in hopes of finding a way to the World of the Forgotten.

Once here in Oubliette, denizens are often confronted with brutal demonstrations of their new immortality. Oubliette is not a friendly place, particularly for newcomers, who are often seen as a source of steady income.

![a female hornfolk denizen](/images/o2e/hornfolk.png)

## The Castle

Castle Oubliette is a humongous agglomeration of thousands of years of decrepit buildings and ancient castles. Nearly a hundred miles from wall to wall, the Castle is divided into districts, wards, and cells. Over the eons, immortal hands have toiled away at rebuilding the city into something slightly greater than a rambling maze of enormous rubble. It's now possible to cross from district to district in relative safety, if one happens to have the coin to pay the innumerable tolls.

![an alley in oubliette](/images/o2e/7807d7e0a92698aa62796a89494c9fe1_original.webp)

There are a great many factions and groups occupying Oubliette. In the absence of direct rule, great swaths of the Castle have been snatched up and are ruled as entirely separate lands.

Because of the undying nature of the denizens of Oubliette, the many of the major factions, including the Draculeans, Coquille Roi, Thorns, and Ordo Sancti, have lasted a very long time indeed. But despite their tenacity, the districts and wards of Oubliette are constantly shifting. New groups such as the Bonehorde and the House of Revision are on the rise, and gaining massive support.

## The Eternal World

Eternal life is a terrible curse, but it does provide one with ample opportunity. Over the eons, Castle Oubliette has been shaped by many hands, transforming entire districts with grinding progress. In Celeste, towers thousands of feet tall loom over the skyline, each one built brick-by-brick from the rubble of discarded centuries. In Mubigild, hundreds of square miles have been carved out from under the sinister surface, home to thousands of tribes of ancient netherfey. Even the comparatively safe confines of Grandhall are home to projects of ridiculous scale, the most famous of which is the Sumeilugrarion, a cathedral-sized organ connected to the entire district by sound tubes and echo chambers, constantly filling the wards with fugues of insane complexity.

![exploration and overland travel in oubliette](/images/o2e/exploration-landscape.png)

There are direct effects, as well. Inhabitants of Oubliette are changed by their time here, often bearing physical manifestations of their newfound powers. Those skilled in the Arts Arcane bear glowing eyes and secondary telekinetic activity and shroud themselves in floating glyphs and magical texts. The Blood Arts are also visible from an early stage in crimson eyes and alabaster skin, but as the power grows, denizens gain terrifying control over their bodies, growing fangs, claws, and even wings when necessary. Savage power begins as a feral rage but quickly progresses into one of the most visually striking of the Arts, leaving practitioner/victims with monstrous forms unique to each individual. These Arts existed in the World of Life, but only in Oubliette do their users find the time, power, and desire to develop them to this extent.

![a giant and a normal sized human woman](/images/o2e/ab64cfa8abd94afa83dbd2fdda7ae50f_original.webp)

The scale of power found in Oubliette is just as astonishing as the variety found there. Most denizens arrive at the height of their mortal power, only to find themselves massively outclassed by their new neighbors. The Broken often slip further down the scale, their capabilities withering until they're barely able to find food to stave off the hunger. The Unbroken usually grab opportunity by the horns and push themselves further up the scale, carving out a greater tale. But the scale continues all the way up to the god-like beings that dwell in Celeste and Slair, who's ancient determination has carried them well beyond the reach of mortal power. And who knows how these fell leaders fare against the great eldritch things that lurk in the darkest caverns of Oubliette?

![Shyri Rahul appears again, as the goddess of the death of universes](/images/o2e/7a2ff1efbd4fdcb659b95b69d9322a75_original.webp)

But the World of the Forgotten is more ancient than human memory. Deep below the darkest bowels of the Castle itself lie ruins of dark secrets from the gulf of prehistory. The Lovecraftian things that dwell there are of such staggering immensity and breathtaking age that they cause tectonic activity in the city above. They've lived here since long before there were people to even make castles.

## The Great Anachronism

Oubliette's timeline doesn't match up with the World of Life. The original castle itself dates to about 1000 AD, but has paradoxically existed in the World of the Forgotten for more than a hundred millennia. A further mystery, when the denizen departed the World of Life holds no bearing on when they arrive in Oubliette. The Castle is home to ancient Greek gods, renaissance-era revolutionaries, dragons from the yawning depths of ages past, and orphans from modern London. Some of the most prodigious minds in the cosmos have set themselves at the task of understanding Oubliette's place in the universe, only to come up empty-handed.

![The golden throne of the Goblin King's court, festering with goblins](/images/o2e/c174df00df536bc8b17b70d0cf025171_original.webp)

Because of this ubiquitous anachronism, all sorts of technologies and stories can be found in Oubliette. Prehistoric shamans rampage past forgotten Soviet era rockets. Soldiers from the Second World War find themselves trapped in an completely-intact early French sanitarium. Cultists of the Great Old Ones rub shoulders with Unseele shadowdancers at a bazaar selling smartphones, now only useful as mirrors. But in spite of all those who wonder, the answer to the question, "Why?" remains elusive.

## System

Oubliette Second Edition runs on the Fate Core rules by Evil Hat, LLC, but because it describes the abilities and traits of the creatures, characters, places, and things within the world in narrative terms, you can easily run it using any generic system you like, such as GURPS, SilCore, or Brevity. We would like to one day produce adapters for Dungeons & Dragons and Apocalypse World/Dungeon World.


## Preview Pages

![oubliette survival, food and water](/images/o2e/o2e_book_preview_01.jpg)
![oubliette castes, forsaken and eminent](/images/o2e/o2e_book_preview_02.jpg)
![oubliette therianthropes](/images/o2e/o2e_book_preview_03.jpg)
![oubliette caste statistics chart and aspects](/images/o2e/o2e_book_preview_04.jpg)
![oubliette strategy and athletics skills](/images/o2e/o2e_book_preview_05.jpg)

## Media

<div class="iframe">
<iframe src="https://www.youtube.com/embed/FvYa6I9MZzY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<br>


<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/270968380&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/aaron-barnes-23" title="AaronBarnesVoiceovers" target="_blank" style="color: #cccccc; text-decoration: none;">AaronBarnesVoiceovers</a> · <a href="https://soundcloud.com/aaron-barnes-23/nerdon-oubliette-kickstarter" title="NerdOn - Oubliette KickStarter" target="_blank" style="color: #cccccc; text-decoration: none;">NerdOn - Oubliette KickStarter</a></div>

## Assets

- [The Gloomhome Murders Kickstarter](https://www.kickstarter.com/projects/josephleebush/the-gloomhome-murders-an-oubliette-adventure-module?ref=evf8oh&token=682e2a19)
  - The Gloomhome Murders [page](/product/o2e/gloomhome)
- Buy Oubliette Second Edition
  - [DriveThruRPG: Print & PDF](https://www.drivethrurpg.com/product/204460/Oubliette-Second-Edition)
- Sheets
  - [Oubliette Character & Campaign Sheets](/downloads/o2e/Oubliette_Sheets.pdf)
  - [Fancy Color Oubliette Sheets](/downloads/o2e/Oubliette_Sheets_Fancy.pdf)
  - [Fillable Oubliette Character & Campaign Sheets](/downloads/o2e/Oubliette_Sheets_fillable.pdf)
  - [Fillable Fancy Color Oubliette Sheets](/downloads/o2e/Oubliette_Sheets_Fancy_fillable.pdf)
- Fiction
  - [Oubliette: First Steps in the Dark](/product/o2e/fsitd)
- Pregen Characters
  - Rank 3 and 4 Pregens for Sea of Lost Paperwork and Gloomhome: <a href="/downloads/o2e/Oubliette_pregens.pdf" download>PDF</a>
