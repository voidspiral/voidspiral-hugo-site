---
title: "The Gloomhome Murders"
date: 2023-07-10T07:00:06-05:00
draft: false
featured: false
tags: [oubliette, module]
featured_image: "/images/o2e/gloomhome-cover.jpg"
logo: "/images/o2e/gloomhome-logo.png"
aliases: ["/oubliette", "/oubliette.html", "o2e"]
authors:
  - Joe Bush
artists:
  - Joe Bush
---

> Three people have died.
> 
> And this time, they've stayed dead.
> 
> As life in Oubliette goes, that's unheard of.

The Gloomhome Murders is an adventure module for Oubliette Second Edition, in which the players are outside investigators brought in to the koom and pnai ward of Gloomhome. This insular area is a tight-knit community fleeing the destruction of their entire planet and solar system, as well as the sinister designs of the dreaded Sunless Church, the extremist bureaucracy that brought on said apocalypse.

<!--more-->

[![](/images/o2e/gloomhome-ks-cover-image.jpg)](https://www.kickstarter.com/projects/josephleebush/the-gloomhome-murders-an-oubliette-adventure-module?ref=evf8oh&token=682e2a19)

---

Back it on [Kickstarter](https://www.kickstarter.com/projects/josephleebush/the-gloomhome-murders-an-oubliette-adventure-module?ref=evf8oh&token=682e2a19) now!

---

Nobody is initially surprised at a few murders. Gloomhome is smack dab in the middle of the district of Deathborne, home to almost all the undead monsters in all of Oubliette. They're beset by powerful enemies who don't appreciate their territory being carved up for the safety of the Gloomish people.

But when it seems clear that the people who've been killed are staying dead, called "Void Death" by denizens, the Council of Elders wants answers. Since the first victim is the only person with experience guarding the ward, they seek outside assistance. They're joined by Deputy Yippra, a young personal assistant completely out of her depth as an investigator on the cases. She needs your help, more than she even knows.

# About Oubliette

In Oubliette, you're already dead. Now, you've got a to figure out what to do with your afterlife. Daily challenges range from finding food that doesn't taste like spongemeat and eyeweed to avoiding Draculean vampire patrols to finding work to avoiding being crushed by the Skyspider's foot as it shifts its position over the city-castle of Oubliette. And that's not even considering all the villains and nightmares who fill every corner of the Great Cage.

Oubliette is the domain of the Forgotten, beings who are exiled from the many different versions of the World of Life to dwell for eternity in a castle spanning hundreds of miles of shambled towers and chaotic elemental powers. The denizens of Oubliette are immortal: each time they are killed, they resurrect somewhere, some time later, frequently in inopportune places at very bad times. This immortality can easily destroy the mind, leaving the majority of the population Broken, people whose minds are utterly destroyed, now laying in the streets and gutters. Do not let that be your fate.

Players in Oubliette can be essentially anything, be it a fallen god, space marine, goblin used car salesman, cosmonaut, elemental firestorm, or something stranger. With the Fate Core system, your character is both unique and flexible.

Oubliette is an enormous 391 page book that has 4.5 stars on DriveThruRPG and is an Electrum Best Seller. It's jam-packed with adventure hooks, skills, stunts, locations, and monsters. Now, witness our dramatic return to Oubliette content with The Gloomhome Murders.

---

Back it on [Kickstarter](https://www.kickstarter.com/projects/josephleebush/the-gloomhome-murders-an-oubliette-adventure-module?ref=evf8oh&token=682e2a19) now!

---

# About the Module

The adventure occurs in 3 distinct phases, Dried Blood, Fresh Blood, and Bound Blood, each representing parts of the investigation separated by major plot points. Those three phases make up the bulk of the book, but a wealth of information has been included before and after them.

Preceding the adventure are chapters introducing the GM to the adventure, the ward of Gloomhome, the people, architecture, plants, and animals that dwell within. We've also included a brief section on setting the proper ambiance with music, lighting, and scent, for those who want a truly immersive experience.

Next is the Dramatis Personae which describes every major character in the adventure, so that the GM can quickly get to know them all and look them up as needed. It also includes a pronunciation guide for each of the characters' names! This chapter also links characters to important segments and their stat blocks if needed.

Next is a separate chapter for character portraits, set aside so that the GM can easily show the players characters without danger of revealing secret parts of the module to them.

The Dried Blood phase currently includes chapters for the 3 already-committed murders, as well as a chapter for sidequests and red-herrings. There's also a short section about the resurrection problem and various outlooks on it. Each of the crime scene chapters includes a description of the location, the scene, and the victim, as well as a number of witnesses. Each has three levels of evidence details: Simple Examination, Detailed Examination, and Outstanding Examination, so that the GM can tailor the information fed to the players based on their rolls and the detail with witch they comb the scene for clues. Some chapters also include a section for possible encounters.

---

Back it on [Kickstarter](https://www.kickstarter.com/projects/josephleebush/the-gloomhome-murders-an-oubliette-adventure-module?ref=evf8oh&token=682e2a19) now!

---

The Fresh Blood phase currently includes chapters for 6 crime scenes that occur through the middle of the story. Here, as the players begin to unravel the story, things get more complicated. Some chapters aren't crime scenes to investigate, but instead are direct investigations of suspicious parties. In this phase, the players are beginning to make suppositions, collect counterfactuals, and generally trim the branches of the tree.

The Bound Blood chapter sees the players engage directly with the threat at hand, and transitions from a murder mystery to something darker and altogether more sinister. As the players reveal the truth behind things, they come ever closer to fearful powers and insidious dangers they could scarcely have anticipated.

Next comes the Denouement, where the players deal with the aftermath and fallout of the investigation.

After that, come the Appendices, one of our favorite parts of an Oubliette product. Here, you'll find some useful tools for the campaign, a Bestiary of new creatures and people the players might run into, and a special Shenanigans appendix to help you (try to) deal with the insane variety of schemes, tricks, and combinations that the players will inevitably deploy in the course of the game.

Currently, without artwork the game is sitting at an even 100 pages. We expect it to grow a bit from the artwork and user-generated content.

You'll find that the module contains a lot of conditional statements. Rather than putting the whole thing on rails, we try to keep the options as open as possible and let the players approach things how they want. We've added a fair number of alternate possibilities, cross-references, and footnotes to help you guide the players along an enjoyable experience for everyone. This flexibility is part of the reason there's a lot of content outside the main chapters of the adventure; they're there to help you stay flexible and avoid getting stalled or going off-book in a bad way.

## Assets
- [The Gloomhome Murders onKickstarter](https://www.kickstarter.com/projects/josephleebush/the-gloomhome-murders-an-oubliette-adventure-module?ref=evf8oh&token=682e2a19)
- [Get Oubliette Second Edition](/product/o2e/o2e)