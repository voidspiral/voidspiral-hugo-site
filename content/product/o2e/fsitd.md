---
title: "Oubliette: First Steps in the Dark"
date: 2017-06-05T12:48:06-05:00
draft: false
featured: false
tags: [oubliette, fiction]
featured_image: "/images/o2e/fsitd-cover.jpg"
logo: "/images/o2e-logo.png"
authors:
  - Richard Kelly
artists:
  - Joe Bush
---

Beneath the universe lies a castle steeped in shadow. Called Oubliette by its denizens, it is a vast and labyrinthine, with massive crumbling provinces ringed by a series of walls that stretch up to the heavens. Its inhabitants are the Forgotten: people, ideas, and even gods that have been discarded by the tangled timelines of the World of Life and left for millennia in the dark.

For some, the Castle is a paradise. Food grows on the walls. The air swirls with magic. Even death is not permanent, most of the time. For others, the Castle is purgatory. Gray mists stifle the daylight. Insular factions wage idiosyncratic wars. Starvation is rampant and death is the least of a person's concerns.

This volume chronicles twelve stories about a place where the body is a costume, belief is a weapon, and monsters walk as men.

In these pages, a young witch wages war on an emperor's dinner menu, a private investigator struggles to solve his own murder, a troll and a kitten supervise an unusual burial, and an expedition to an abandoned laundromat does not go as planned.

---

Featuring revised content from the original Kickstarter campaign and additional material commissioned by backers, First Steps In The Dark is a braided anthology, with each story weaving into the next to form a complete chronicle of the events that will happen during Year One of the World of the Forgotten.

## Assets

- Buy Oubliette: First Steps in the Dark
  - [DriveThruRPG: Print & Digital](https://www.drivethrurpg.com/product/212142/Oubliette-First-Steps-in-the-Dark)
