---
title: "America: Land of Monsters"
date: 2018-04-01T12:48:00-05:00
draft: false
tags: [hfa, expansion]
featured: false
featured_image: "/images/hfa/alm-cover.jpg"
logo: "/images/alm-logo.png"
hidehero: false
authors:
  - Joe Bush
artists:
  - Joe Bush
---

<sub class="subtle">Requires [Heroines of the First Age](/product/hfa/hfa) to play!</sub>

*Enter Post-Surge America, where racial tensions are at an all time high, immigration is a matter of immediate life and death for all involved, and you'd kill for a job at the burger joint around the corner.*

![An oni monstergirl police officer](/images/hfa/cop.png)

**We are the Newcomers!** We've fled from the destruction of our worlds and stumbled onto Earth, circa 2018. Our new lives are complicated by our demi-human traits and our cultural differences, but we must preservere.

**America is Hard!** Nobody is waiting to tell us how this new world works. Legal issues stand in the way of our citizenship at every turn. Corporations are leery of investing in us. Jobs are hard to get when your hands are claws.

**Big Business is Scary!** While we've escaped the obvious evil of The Great Dark One, we're unprepared for the insidious control the corporate world exerts on us all. Are we consumers, products, or both?

![Never give your adopted catgirl daughter your credit card](/images/hfa/card.jpg)

**Corruption is Rampant!** Behind a veneer of politeness, everyone's trying to get something. Who can you trust when so much of our livelihoods depends on social currency we weren't given in the first place, or money we're not allowed or able to earn?

**Our Stories are Wild!** Life ranges from amusingly disastrous to sickeningly grotesque. Which will our story be? Will we struggle to keep a stable job or struggle to keep our sanity?

America: Land of Monsters is an expansion for [Heroines of the First Age](/product/hfa/hfa), with a fully-fleshed setting, 7 Modern Archetypes, 24 new adversaries, and extensive rules for dealing with the hardest parts of life in the States.

*Trigger Warning: Racism, Domestic Violence, Discrimination, Sex, Profanity*

![A blue water dragon monstergirl lawyer](/images/hfa/lawyer.png)

## Sample Pages

![The sliding scale of terror vs hilarity](/images/hfa/alm_page1.png)
![crapsaccharine](/images/hfa/alm_page2.png)
![a jerk waiting for a be-tentacled gas station attendant](/images/hfa/store.jpg)
![Villainess with a nobelwoman's laugh](/images/hfa/nanami.jpg)

## Assets

- [Get HFA Here](/product/hfa/hfa)
- Get America: Land of Monsters
  - [DriveThruRPG: Print & PDF](https://www.drivethrurpg.com/product/243401/America-Land-of-Monsters)
- [ALM Archetype Sheet](/downloads/hfa/ALM_Archetypes.pdf)
- [ALM Move Sheet](/downloads/hfa/ALM_Moves.pdf)