---
title: "Heroines of the First Age"
date: 2017-09-27T12:48:00-05:00
draft: false
tags: [hfa, core, game]
featured: false
featured_image: "/images/hfa/hfa-cover.jpg"
logo: "/images/hf-logo.png"
hidehero: false
aliases: ["/heroines-of-the-first-age", "/heroines of the first age", "/hfa"]
authors:
  - Joe Bush
artists:
  - Joe Bush
---

> Sing, Muse, of Xlu the Red, of Tyakua the Stonebound, of Elbor the Unjust. Sing of the Heroines of the First Age, of the fire at the beginning of the world.

HFA is a tabletop role-playing game about larger-than-life monsters and the dawn of civilization. HFA is the intersection between monstergirls, RPGs, and myth & legend.

<!--more-->

* Powered by the Apocalypse
* Playtest Packet Available
* Collaborative World Building
* Variety of Archetypes: Sorceress, Soldier, Shadewalker, Outlander, Socialite, Priest, Companion
* War Moves & Epic Combat
* Equipment, upgrades, and enchantments
* Creature Portfolios and Power Pools threaten the world

Inspirational Media: Nibelinglied, Maoyusha, Wintersun, The Epic of Gilgamesh, Monster Musume, Conan the Barbarian, Beowulf, Kobayashi chi no Maid Dragon, Black God's Kiss, Everyday Life with Monstergirls, The Illiad, 12 Beast, Enuma Elish, Myth, & Legend.

**We are the Heroines!** Demi-humans, monsterfolk, beastmen, whatever you call us, this is our story. We have many forms, from kistune to arachne, from centaur to harpy. We stand against terrible, vengeful gods and unstoppable cosmic forces.

**This is the First Age!** It is the dawn of civilization. People are just coming out of the forests and caves to create towns and agriculture. Grand armies are being assembled for the first time in history. The world is fresh, new, and exciting, and there's so much of it that has never been explored before.

**Lead Armies!** Take command of military units and field them against your enemies, or make a stand against entire armies yourself.

**Prepare for your Quest!** Heroines of the First Age contains 10 Archetypes, dozens of special powers, and more Moves than even the gods can handle. Choose from more than 20 Tragic Flaws and select a panoply of equipment fit for royalty.

**Discover and Build as You Go!** Collaborative worldbuilding at its best! Work together to create your very own tale! Heroines of the First Age contains everything you need to craft a unique world with your friends!

About HFARPG
Heroines of the First Age isn't about uncovering ancient secrets and defeating primordial powers. In Heroines of the First Age, you are the primordial powers. You make those ancient secrets. This is the First Age, writ large, and the world is yours to make or destroy. Your actions will shape the world for eons to come.

![Heroines of the First Age characters](/images/hfa/roster-3.jpg)

## Features

- Modular Character Creation!
- Customize your monstrous traits!
- Powered by the Apocalypse!
- Collaborative world building!
- Wide Variety of Archetypes!
- War Moves & Large Scale Combat!
- Equipment, Upgrades, and Blessings!
- Creature Portfolios to fill out the enemy ranks!
- Power Pools to form the basis for cosmic metaphysics!

![An eyebeast-girl](/images/hfa/beholdr.png)

## Playing HFARPG

HFA is designed to be a collaborative storytelling experience. The GM guides the story and provides challenges, but will often ask the players to fill in gaps in the lore or to answer questions about how the game world works. They've got a lot on their plate, so help them out by thinking up things that will make the game more interesting for everyone. Since we're all here for entertainment, all of us have to pitch in to make the game fun and exciting for all.

Most of the rules in Heroines of the First Age are character-centric meaning they revolve around the PCs rather than the many enemies, NPCs, environments, challenges, and hazards they encounter. The GM has certain rules to follow as well, but most of them revolve around providing the right amount and kind of resistance to the advance of the PCs.

![A slime-lamia crossbreed witch](/images/hfa/slime.png)

Heroines of the First Age is like a conversation. Players say what they want their PCs to do and the GM responds, usually by suggesting a particular Move or by describing the situation. The GM always finishes up their end by asking "what do you do?" to keep the game moving forward.

It's important to note that there are no "turns" in Heroines of the First Age. The enemies do not have their own actions to perform. Instead, success and failure are in the hands of the PCs. Each time a PC attempts to do something, the outcome will be one of three things: Success, Tie, or Failure. When the PCs look to the GM to see what happens next, the GM Moves. When a PC fails something, the consequences are more severe, often in the form of damage or disruption of the PCs' plans.

![A harpy](/images/hfa/harpy.png)

## PCs

PCs are mostly composed of five parts:

- Vitals describe your character's appearance and basic traits.
- Primary Stats describe your character's base abilities, how good they are at various kinds of tasks.
- Secondary Stats describe things like how tough or experienced your character is, how much stuff they have, and their relationships with others.
- Moves are the specific things your character can do. Each Move has its own mechanics. Some are offensive while others are utilitarian.
- Equipment is the stuff you character has and uses. It encompasses everything from Weapons and Armor to Resources and even followers.

![a many-armed goddess](/images/hfa/fate.jpg)

## GMing

The GM's rules are somewhat simpler because they're juggling more at a time.

- Agenda is sort of the why of running the game and informs the reasoning for your Principles and Moves.
- Principles are the ins and outs of how to run the game, what you should generally be trying to do.
- GM Moves are the individual tools you use to follow your Agenda and adhere to your Principles. These are the things you do when fate intervenes in the PCs' lives.
- Adversaries are the enemies, villains, and NPCs that the PCs will run up against. Adversaries come in two flavors and have their own GM Moves and Principles.

## Photos

![A photo of the printed book, cover](/images/hfa/photo-cover.jpg)
![A photo of the printed book, tables](/images/hfa/photo-charts.jpg)
![A photo of the printed book, foxcat monstergirl](/images/hfa/photo-foxcat.jpg)
![A photo of the printed book, powers](/images/hfa/photo-powers.jpg)
![A photo of the printed book, journey through landscape](/images/hfa/photo-journey.jpg)

## Kickstarter Video

<div class="iframe">
<iframe src="https://www.youtube.com/embed/sqPBCVH8jVc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

## Assets
- Buy HFA
	- [DriveThruRPG: Print & Digital](https://www.drivethrurpg.com/product/222629/Heroines-of-the-First-Age)
- Sheets
	- [Character Sheet](/downloads/hfa/HFA_PC_Character_Sheet.pdf)
	- [Character Sheet, Fillable](/downloads/hfa/HFA_PC_Character_Sheet_fillable.pdf)
	- [PC Archetype Moves](/downloads/hfa/HFA_PC_Archetype_Moves.pdf)
	- [PC Moves](/downloads/hfa/HFA_PC_Moves.pdf)
	- [HFA Game Prep](/downloads/hfa/HFA_Game_Prep.pdf)
	- [HFA handouts and GM screen](/downloads/hfa/HFA_handouts_and_GM_screen.pdf)
- Expansions
	- [Heroines of the Last Age](/product/hfa/hla)
	- [America: Land of Monsters](/product/hfa/alm)
	- [Heroines of the First Age: Holdfast](/product/hfa/holdfast)
	- [Heroines of the First Age: The War of the Prophecy](/product/hfa/pw)
- [Errata](/post/hfa-errata)

![various monstergirl sketches](/images/hfa/sketches.jpg)

<sub class="subtle centered">**Warning:** This game is very pink.</sub>