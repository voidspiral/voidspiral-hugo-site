---
title: "Holdfast"
date: 2018-01-01T12:48:00-05:00
draft: false
tags: [hfa, expansion]
featured: false
featured_image: "/images/hfa/hf-cover.jpg"
logo: "/images/hfa-logo.png"
hidehero: false
authors:
  - Richard Kelly
  - Joe Bush
artists:
  - Joe Bush
---

<sub class="subtle">Requires [Heroines of the First Age](/product/hfa/hfa) to play!</sub>

*Enter an epic fantasy tabletop RPG about reincarnated monsters, endangered gods, and mutually assured destruction on the last world in existence.*

![The Multigoddess shapes the new world of Holdfast](/images/hfa/hf2.jpg)

**We are what remains of the Five Worlds.** Forty years ago our gods destroyed our homes, killing themselves in the process. We escaped to Holdfast, a tiny speck of a world, where we could lie low and rebuild. This was not to be.

**We have nowhere left to run.** Holdfast is in peril. Our savior, the Multigoddess, lies dormant or dead. Our nations are at each other's throats. Our new gods are small, weakened things, and we are at war over whether we ought to exterminate them.

**We are chained by our pasts.** Many of us died with the Five Worlds. Others were snatched from the void and jammed into new bodies. Some of us have forgotten who they used to be. Others remember every grudge they held.

![A kudu Warden of the Many, a wyvern Favored of Owls, and a rebuilt Apotheotic Convergent, and one grumpy-ass turtle](/images/hfa/hf5.jpg)

**But we will not surrender.** We are not the same people we were forty years ago. Our heroines now cross the sky in domesticated Bulbships. They bind themselves to spirits, gods, and Megabeasts. They swear their services to new powers and new nations.

**Together, we have a chance to survive.** Arrayed against us are zealots and reality-saboteurs, gods and monsters, broken relics and the sins of our mothers. We will face them as sisters or we will die on each other's blades.

**Heroines of the First Age: Holdfast** is an expansion for Heroines of the First Age, with a fully-fleshed setting and 21 new Races, 7 Archetypes, 70 Moves, and 46 Factions, Antagonists, Power Pools, and Creature Portfolios.

## Sample Pages

![The map of Holdfast](/images/hfa/HF-map.jpg)
![The Four Gods and the Names of the Goddess](/images/hfa/hf-pdf-1-copy.jpg)
![The World of Holdfast & NPCs](/images/hfa/hf-pdf-2-copy.jpg)
![A a seedship captain loading trade wares](/images/hfa/hf6.jpg)

## Assets

- [Get HFA Here](/product/hfa/hfa)
- Get Holdfast
  - [DriveThruRPG: Print & PDF](https://www.drivethrurpg.com/product/242461/Heroines-of-the-First-Age-Holdfast)
- [Holdfast Archetype Sheets](/downloads/hfa/Holdfast_Archetype_Moves.pdf)
- [Holdfast Move Sheets](/downloads/hfa/Holdfast_Moves.pdf)
- Check out the prequel, [War of the Prophecy](/products/hfa/pw)