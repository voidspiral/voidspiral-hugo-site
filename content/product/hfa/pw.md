---
title: "War of the Prophecy"
date: 2018-07-26T12:48:00-05:00
draft: false
tags: [hfa, fiction]
featured: false
featured_image: "/images/hfa/pw-cover.jpg"
logo: "/images/alm-logo.png"
hidehero: false
authors:
  - Richard Kelly
artists:
  - Joe Bush
---

<sub class="subtle">A prequel to [Holdfast](/product/hfa/holdfast)</sub>

On the same day, at the same time, the priestesses of the Five Worlds all announce the same news: the Worlds are scheduled to be remade. From among the mortal populations, a goddess will be chosen, and she alone will determine what happens to them.

What follows next is a tale of predatory gods, warring nations, and larger than life heroines at the dawn of time.

Featuring a revised text and exclusive bonus content, The Prophecy War is a reprinting of the high fantasy serial that was released during the successful Heroines of the First Age Kickstarter campaign. Although a standalone story, the events of The Prophecy War also serve as a direct prequel to the setting supplement, Heroines of the First Age: Holdfast.

## Assets

- [Get HFA Here](/products/hfa/hfa)
- [Get Holdfast Here](/products/hfa/holdfast)
- Get War of the Prophecy
  - [DriveThruRPG: Print & Digital](https://www.drivethrurpg.com/product/247824/HFA-The-War-of-the-Prophecy)