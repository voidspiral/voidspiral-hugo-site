---
title: "Heroines of the Last Age"
date: 2018-02-01T12:48:00-05:00
draft: false
tags: [hfa, expansion]
featured: false
featured_image: "/images/hfa/hla-cover.jpg"
logo: "/images/hla-logo.png"
hidehero: false
authors:
  - Joe Bush
artists:
  - Joe Bush
---

<sub class="subtle">Requires [Heroines of the First Age](/product/hfa/hfa) to play!</sub>

*Enter an epic science fiction tabletop RPG about a pinnacle metaspieces poised on the knife edge between decline and transcendence!*

![a dragongirl space fighter pilot samurai](/images/hfa/elegant.jpg)

**We are the Hama.** Our systems span the galaxy. Our weapons can save systems or doom them. Our sciences are a match for any other civilized species we encounter.

**This is the Last Age.** We are tired. For two billion years we have resisted The Churn, that galactic tide of civilizations that relentlessly washes against the shores of history. We thought we had exhausted all the possibilities of the universe, then it happened.

**Heartdrive.** Developed in secret and distributed to all Hama equally, the technology of FTL has opened up a whole new epoch of our history, a new era marked by the explorations of distant spaces we could barely even imagine before.

**Our Enemies Surround Us.** Within and without, we are beset by those who would like nothing more than to make the Hama's reign through the galaxy a historical footnote. We no longer share the sentiment. We will remind them of our strength.

**Galactic Mysteries Abound.** While we've already scoured our home systems for the valuable technologies of ancient cultures, The Heartdrive has opened up our ability to explore countless strange phenomena and interact with dozens of transcendent powers. We are the first wave of these new explorers.

**Heroines of the Last Age** is an expansion for [Heroines of the First Age](/product/hfa/hfa), with a deeply detailed galaxy filled with almost 50 unique worlds, dozens of new enemy factions and cultures, 10 galactic-scale super-villains, and a whole new suite of archetypes, equipment, and spacecraft for you to tool around the galaxy in.

## Sample Pages

![the hyper-tyrant leader of a fleet of villains](/images/hfa/adversary.jpg)
![inhabited planets in a nebula](/images/hfa/planets.jpg)
![A green photosynthesizing hama settler enjoying the meagre light of a dim star](/images/hfa/settler.jpg)
![A space marine](/images/hfa/scifi_sketch.jpg)
![Galactic Mysteries](/images/hfa/hla1.png)
![Common Operations & The Cartography Service](/images/hfa/hla2.png)
![A battleship emerging from an energetic nebula](/images/hfa/battleship.jpg)

## Assets

- [Get HFA Here](/product/hfa/hfa)
- [Get Heroines of the Last Age]
  - [DriveThruRPG: Print & PDF](https://www.drivethrurpg.com/product/246519/Heroines-of-the-Last-Age)
- [HLA Archetype Sheets](/downloads/hfa/HLA_Archetypes.pdf)
- [HLA Move Sheets](/downloads/hfa/HLA_Moves.pdf)