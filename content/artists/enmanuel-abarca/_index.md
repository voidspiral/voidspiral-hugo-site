---
title: Enmanuel Abarca
featured_image: ""
aliases: []
---

Enmanuel Abarca is an artist from Venezuela living in Colombia. He has been a freelance artist for about 7 years and a full time artist for about 3 years now. He has been drawing since he can remember, and it has always been his passion and his hobby, but it never came across his mind that he would been doing it for a living. He studies graphic design and, although it’s not directly related to illustration, he did learn a lot of color, composition and how to use most of the programs that he uses today. Illustration has been mostly a self taught process, and he has worked mostly on small independent publisher projects, book covers, and a lot of stockart. Here you can find his [online portfolio](https://www.artstation.com/gmlemas).