---
title: Miguel Santos
featured_image: ""
aliases: []
---

Miguel Santos is a freelance illustrator for RPGs, tabletop/card games, and magazines. He is also an indie comic book author. His favorite themes are Sci Fi, Horror, Fantasy, History, and Politics. His dystopian webcomic is here, and his online portfolio can be found [here](http://pictishscout.daportfolio.com/).