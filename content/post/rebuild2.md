---
title: "Rebuilding the Voidspiral Website"
date: 2020-05-17T12:55:06-05:00
draft: false
type: "post"
tags: [meta]
---

It's been three months since I started writing this post. In the mean time, I've learned a whole lot of Hugo, rebuild my own personal site, [Arcandio.com](https://arcandio.com) and learned a lot about automated static site generation, continuous integration, and Gitlab Pages.

As we draw nearer to the new website being ready for prime time, I figured I'd explain a bit about how it works and what I intend to put here.

The new site runs on the [Hugo](https://gohugo.io/) static site generator, which means it doesn't use any server-side scripting at all. It's completely static, the pages are generated at compile time (continuous integration with gitlab pages) and then the generated pages are served directly to the client. This has the advantage of not really needing a server, and also allowing me a lot of freedom to design the site how I want. Conversely, that means I have a lot to maintain if something goes wrong.

> If something *does* go wrong, please report it to us on the [Discord](https://discord.gg/r2rPW5c).

Hugo allows us to organize content into whatever structure we want. In our case, we have a [product](/product/) section that holds all the products Voidspiral has made. Right now, the database only has the games &  a few assets, but none of the fonts, maps, or art packs. It'd also be nice to have the ISBNs and SKUs for each product on the page, but I don't know where to put them yet. And I'd post about the business of making games (such as I know it) here on the website too, just because I think it's interesting and it's so hard to find information about how people execute their vision these days.

This verison of the website should be more easily maintainable and have better SEO than the previous one, which was based on a local Ghost installation, and needed a huge amount of screwing around to publish new content, making it no better than the carrd.co website before that.

So here's to a new Voidspiral website, which hopefully will last for a *long* time.