---
title: "TDL Errata"
date: 2019-03-19T12:48:00-05:00
draft: false
tags: [tdl, errata]
featured: false
featured_image: "/images/tdl/tdl-logo.png"
hidehero: false
---

## [The Dawnline](/product/tdl/tdl)

- Page 8 “if it was willing to play nice” should be play instead of place
- Page 21 “when everyone else has their characters ready” should be ready instead of reading