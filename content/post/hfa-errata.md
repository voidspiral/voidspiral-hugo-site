---
title: "HFA Errata"
date: 2017-09-27T12:48:00-05:00
draft: false
tags: [hfa, errata]
featured: false
featured_image: "/images/hfa/hfa-logo.png"
hidehero: false
---

## [Heroines of the First Age](/product/hfa/hfa)

- 5 several spaces snuck in between commas and names.
- 42 Mindreader should say Alertness rather than Cunning.
- 49 "spirt" should be spirit.
- 58 Super Ability: this move is mostly for non-combat, world-interaction types of things, like a Risky Prop on steroids. It gives you narrative permission to handle certain tasks with a single brute-force method.
- 28 says that you start with 3 willpower stars, which is incorrect. It should be 2. 73 says you start with 2 willpower, which is correct. We were mistaken about this in previous errata. You don't need a lot of Willpower in order to avoid the worst effects of your flaws in a regular session, so 2 suits the system better for starting characters. You can always buy more.
- 82 Impressive Tag on Disguised Armor?
  - Yes, this is as intended. It's fancy clothes with disguised armor.
- 89 Stories and Songs: missing end parenthesis.
- You start with up to 4 relationships, but you are not limited to 4. The GM can assign new relationships, and players can suggest them as makes sense in the fiction. When you resolve one, you should replace it with a new one.

## [Heroines of the Last Age](/product/hfa/hla)

- HLA 283 various typos
  - remove "to" in  "but to unleashes"
  - "Flowlight Claws & Btite" should be "Flowlight Claws & Bite"
