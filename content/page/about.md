---
title: "About"
date: 2020-05-17T19:24:08-05:00
description: ""
tags: [meta]
draft: false
featured_image: "/images/Voidspiral-Gold-transp-sm.png"
---

Voidspiral Entertainment is a tiny creative properties company that makes tabletop games, fiction, board games, and gaming assets. We're based in Madison, Wisconsin. You can mostly find our games on DriveThruRPG.

Voidspiral Entertainment began back in 2008, when we started working on the first edition of Oubliette, before which might have been a previous project called Legends of Culture that yet awaits is time in the limelight.

Voidspiral is generally composed of a single person, Joe Bush, with the occasional assistance from Elizabeth LeBouton, Richard Kelly, Ian Hamilton, and a host of others.

To meet the creative folks behind the scenes, check out our [authors](/authors) and [artists](/artists).