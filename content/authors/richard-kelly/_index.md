---
title: Richard Kelly
featured_image: "/images/avatars/richard-preen.png"
weight: -90
---

Richard Kelly is a writer and designer whose interests run towards weird and gothic fiction. The game lines he has written for include Oubliette Second Edition, Heroines of the First Age, Pathogen: Unclassified, Wicked Pacts, Splinter, and Golden Sky Stories. He has been interested in Japanese-style tabletop ever since discovering that it exists, and his writing is occasionally infested with owls.

- [Website](https://kumada1.itch.io/)