---
title: Joe Bush
featured_image: "/images/avatars/joe-dragon.jpg"
aliases: ["/artists/joe-bush/"]
weight: -100
---

Joseph Lee Bush is a game designer and artist known for Heroines of the First Age, Oubliette, Summoner's Grimiore, and Wasuremonogatari.

Artistically, he works almost exclusively in digital art, favoring stylized work with a distinct anime and manga feel. Joe's projects range through fantasy, science fiction, horror, and Japanese themes.

He is the owner of Voidspiral Entertainment where he works full time on game development and commissions.

You can also view his [artist page here.](/artists/joe-bush)

- [Website](https://arcandio.com)
- [Gitlab](https://gitlab.com/arcandio)
- [Instagram](https://www.instagram.com/arcandio/)
- [Twitch](https://www.twitch.tv/arcandioart)
- [DriveThruRPG](https://www.drivethrurpg.com/browse.php?artist=Joe%20Bush)
- [Twitter](https://twitter.com/Voidspiral)
- [Mastodon](http://tabletop.social/@voidspiral)
- [Facebook](https://www.facebook.com/JosephLeeBush)